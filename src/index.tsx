import * as React from 'react'
import { render } from 'react-dom'
import App from './App'

/* CSS */
import './css/main.css'
import './css/__anchors.css'
import './css/__padding.css'
import './css/__textAlign.css'
import './css/__typography.css'

render(<App />, document.getElementById('root'))

console.log(`You opened the console!`)
