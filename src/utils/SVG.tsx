import * as React from 'react'

interface SVGProps {
  src: string
}

const SVG = ({ src }: SVGProps) => (
  <span dangerouslySetInnerHTML={{ __html: src }}></span>
)

export default SVG
