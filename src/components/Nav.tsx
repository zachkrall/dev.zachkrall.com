import * as React from 'react'

import './Nav.css'

const Nav = () => (
  <nav>
    <ul>
      <li>
        <a className="pill" href="https://zachkrall.com">
          &larr; Homepage
        </a>
      </li>
    </ul>
    <ul>
      <li>
        <a className="pill" href="/zach-krall_resume.pdf">
          Download PDF
        </a>
      </li>
      <li>
        <a
          className="pill"
          href="mailto:zach@zachkrall.com?subject=You're Hired!"
        >
          Contact
        </a>
      </li>
    </ul>
  </nav>
)

export default Nav
