import * as React from 'react'

const FeaturedIn = () => (
  <div>
    <div className="pb3 measure f4 lh-title">
      Press:
      <a
        href="https://www.nytimes.com/2019/10/04/style/live-code-music.html"
        className="link bb b--black-30 black hover-white"
      >
        The New York Times
      </a>
      ,
      <a
        href="https://hackernoon.com/hydra-tutorial-for-live-coding-visuals-1h4532qa"
        className="link bb b--black-30 black hover-white"
      >
        Hacker Noon
      </a>
      ,
      <a
        href="https://joy.recurse.com/posts/722-p5snap"
        className="link bb b--black-30 black hover-white"
      >
        Joy of Computing
      </a>
      ,
      <a
        href="https://monthlymusichackathon.org/post/183904707222/live-code-lab-recap-resources"
        className="link bb b--black-30 black hover-white"
      >
        Monthly Music Hackathon
      </a>
    </div>
    <div className=" pt2 measure f4 lh-title">
      Alumnus:
      <a
        href="https://recurse.com"
        className="link bb b--black-30 black hover-white"
      >
        Recurse Center
      </a>
      ,
      <a
        href="https://www.newschool.edu/parsons/mfa-design-technology/"
        className="link bb b--black-30 black hover-white"
      >
        Parsons MFADT
      </a>
      ,
      <a
        href="https://artahack.io/alumni"
        className="link bb b--black-30 black hover-white"
      >
        Art-A-Hack
      </a>
    </div>
  </div>
)

export default FeaturedIn
