import * as React from 'react'

import './Workshops.css'

interface WorkshopEntry {
  title: string
  org: string
  date: string
}

interface WorkshopProps {
  items: Array<WorkshopEntry>
}

const Workshops = ({ items }: WorkshopProps) => (
  <div className="pa workshops">
    <h2>Workshops</h2>
    <div className="grid">
      {items.map(({ title, org, date }: WorkshopEntry, k: number) => (
        <React.Fragment key={String(k)}>
          <div>{org}</div>
          <div>{title}</div>
          <div>{date}</div>
        </React.Fragment>
      ))}
    </div>
  </div>
)

export default Workshops
