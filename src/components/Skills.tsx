import * as React from 'react'

import './Skills.css'

interface Skill {
  [key: string]: Array<string>
}
interface SkillsProps {
  items: Skill
}

const Skills = (props: SkillsProps) => (
  <div className="pa skills">
    {Object.keys(props.items).map((k: string, i: number) => (
      <div key={k + String(i)}>
        <h2>{k}</h2>
        {props.items[k].map((entry: string) => (
          <span>{entry}</span>
        ))}
      </div>
    ))}
  </div>
)

export default Skills
