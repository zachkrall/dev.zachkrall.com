import * as React from 'react'

import './List.css'

interface ListProps {
  items: any
  header: string
}

interface ListEntry {
  title: any
  url: any
  image: string
  description: any
  lang: any
}

const List = ({ items, header }: ListProps) => (
  <div className="pa list">
    <h2>{header}</h2>
    <div className="grid">
      {items.map(
        ({ title, url, image, description, lang }: ListEntry, key: number) => (
          <a key={key} href={url} className="card">
            {(lang || image) && (
              <div
                className="image"
                style={{ backgroundImage: `url('./static/images/${image}')` }}
              ></div>
            )}
            <div className="ph">
              <div className="title">{title}</div>
              <div className="description">{description}</div>
            </div>
          </a>
        )
      )}
    </div>
  </div>
)

export default List
