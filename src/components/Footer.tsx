import * as React from 'react'

const CurrentYear = (): string => {
  return new Date().getFullYear().toString()
}

const Footer = () => (
  <footer className="pa">
    <div class="tc" style={{ marginBottom: '2rem' }}>
      If you're interested in learning more, &nbsp;
      <a href="mailto:zach@zachkrall.com" className="pill">
        Say Hi 👋
      </a>
    </div>
    <div className="pb mono small">
      Built with&nbsp;
      <a href="https://www.typescriptlang.org/">TypeScript</a>,&nbsp;
      <a href="https://reactjs.org">React</a>, and&nbsp;
      <a href="https://rollupjs.org/">Rollup</a>. (
      <a href="https://gitlab.com/zachkrall/dev.zachkrall.com/">Source Code</a>)
    </div>
    <div className="small mono">
      &copy; Copyright {CurrentYear} Zachary Krall. All Rights Reserved.
    </div>
  </footer>
)

export default Footer
