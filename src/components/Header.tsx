import * as React from 'react'

// import FeaturedIn from './FeaturedIn'

import SVG from '../utils/SVG'
import githubsvg from '../svg/github.svg'
import linkedinsvg from '../svg/linkedin.svg'

import './Header.css'

const Header = () => (
  <header>
    <div className="pa fade-in">
      Zach Krall is a <span>Front-End Developer</span>, <span>UI Engineer</span>
      , and <span>Creative Coding Educator</span> with an MFA in Design +
      Technology from Parsons School of Design.
    </div>
    <div className="pa">
      <ul className="features">
        <li>Open to Relocating</li>
        <li>Actively Interviewing</li>
        <li>Does not require visa sponsorship</li>
      </ul>
    </div>
    <div class="pa">
      <ul className="social">
        <li>
          <a href="https://github.com/zachkrall">
            <SVG src={githubsvg} />
          </a>
        </li>
        <li>
          <a href="https://linkedin.com/in/zachkrall">
            <SVG src={linkedinsvg} />
          </a>
        </li>
      </ul>
    </div>
    {/* <FeaturedIn /> */}
  </header>
)

export default Header
