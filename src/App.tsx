import * as React from 'react'

import Data from './data.json'

import Nav from './components/Nav'
import Header from './components/Header'
import List from './components/List'
import Skills from './components/Skills'
import Workshops from './components/Workshops'
import Footer from './components/Footer'

function App() {
  return (
    <div id="app">
      <div class="max-width">
        Coming Soon
        {/* <Nav />
        <Header />
        <List items={Data['Open Source']} header="Open Source" />
        <List items={Data['Projects']} header="Projects" />
        <Skills items={Data['Skills']} />
        <List items={Data['Talks']} header="Talks" />
        <Workshops items={Data['Workshops']} />
        <Footer /> */}
      </div>
    </div>
  )
}

export default App
