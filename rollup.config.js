import serve from 'rollup-plugin-serve'
import typescript from 'rollup-plugin-typescript'
import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import replace from '@rollup/plugin-replace'
import cssbundle from 'rollup-plugin-css-bundle'
import json from '@rollup/plugin-json'
import svg from 'rollup-plugin-svg'

const devServer = () => {
  if (process.env.NODE_ENV === 'Development') {
    return serve({
      open: false,
      contentBase: 'public',
      host: 'localhost',
      port: 3000
    })
  } else {
    return null
  }
}

export default {
  input: './src/index.tsx',
  output: {
    file: './public/bundle.js',
    format: 'cjs'
  },
  plugins: [
    // ------ DEV SERVER       ------
    devServer(),
    // ------ LANGUAGE SUPPORT ------
    typescript(),
    // ------ ASSET SUPPORT    ------
    cssbundle(),
    json(),
    svg(),
    // ------ ENVIRONMENT      ------
    resolve({
      browser: true
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    commonjs({
      transformMixedEsModules: true,
      namedExports: {
        'node_modules/react/index.js': [
          'Children',
          'Component',
          'PropTypes',
          'createElement',
          'Fragment'
        ],
        'node_modules/react-dom/index.js': ['render']
      }
    })
  ]
}
